package pl.edu.agh.iosr.crawler.crawlerservice.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.agh.iosr.crawler.crawlerservice.client.AgregateClient;
import pl.edu.agh.iosr.crawler.crawlerservice.domain.BookPortion;
import pl.edu.agh.iosr.crawler.crawlerservice.domain.SimpleBook;
import pl.edu.agh.iosr.crawler.crawlerservice.service.CrawlerService;

import java.util.ArrayList;


/**
 * Created by user on 07.12.2017.
 */
@EnableAutoConfiguration
@ComponentScan
@RestController
@RefreshScope
public class CrawlerController {

    @Value("${info.message}")
    private String value;
    @Value("${scrapper.size}")
    private Integer size;
    @Value("${scrapper.follow}")
    private Boolean follow;

    @Autowired
    private CrawlerService crawlerService;
    private AgregateClient bookMatcher;
    //private String url = "https://katalog.mbp-oswiecim.pl/webmak/wyszukiwanie.html";
    private String url ="http://data.bn.org.pl/api/bibs.json?limit=100&";
    private static final Logger LOG = LoggerFactory.getLogger(CrawlerController.class);

    @RequestMapping(path = "/title:{value}", method = RequestMethod.GET)
    public ArrayList<SimpleBook> crawlByTitle( @PathVariable String value) throws Exception {
        ArrayList<SimpleBook> bookList;
        bookList = crawlerService.scrapUrl(url +"title=" + value,true);
        return bookList;
    }

    @RequestMapping(path = "/info", method = RequestMethod.GET)
    public String  configTest() throws Exception {

        return value;
    }

    @RequestMapping(path = "/isbn:{value}", method = RequestMethod.GET)
    public ArrayList<SimpleBook> crawlByIsbn(@PathVariable String value) throws Exception {
        ArrayList<SimpleBook> bookList;
        bookList = crawlerService.scrapUrl(url +"isbnlssn=" + value,false);
        return bookList;
    }

    @RequestMapping(path = "/yearAll:{value}", method = RequestMethod.GET)
    public ArrayList<SimpleBook> crawlByYearAll(@PathVariable String value) throws Exception {
        ArrayList<SimpleBook> bookList;
        bookList = crawlerService.scrapUrl(url +"publicationYear=" + value,follow);
        return bookList;
    }

    @RequestMapping(path = "/year:{value}/{since}", method = RequestMethod.GET)
    public BookPortion crawlByYearPortion(@PathVariable String value,@PathVariable String since) throws Exception {
        BookPortion portion = new BookPortion();
        portion = crawlerService.scrapUrlPortion(url +"publicationYear=" + value+"&sinceId="+since,size);
        return portion;
    }


    @RequestMapping(path = "/custom:{key}={value}", method = RequestMethod.GET)
    public ArrayList<SimpleBook> crawlCustom(@PathVariable String key, @PathVariable String value) throws Exception {
        ArrayList<SimpleBook> bookList;
        bookList = crawlerService.scrapUrl(url + key + "=" + value,follow);
        return bookList;
    }
}
