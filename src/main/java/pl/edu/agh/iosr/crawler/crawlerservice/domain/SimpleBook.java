package pl.edu.agh.iosr.crawler.crawlerservice.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by user on 07.12.2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleBook {

    @JsonProperty("title")
    private String title;

    @JsonProperty("author")
    private String author;

    @JsonProperty("publisher")
    private String publisher;

    @JsonProperty("publicationYear")
    private String year;

    private boolean available = true;

    public String getTitle() {
        return title;
    }

    public SimpleBook(){

    }
    public SimpleBook(String title, String author, String publisher, String year) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.year = year;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public String toString(){
        return "{title: " + title +"; author: "+author+"; publisher: "+publisher+";year: "+year+"}";
    }
}
