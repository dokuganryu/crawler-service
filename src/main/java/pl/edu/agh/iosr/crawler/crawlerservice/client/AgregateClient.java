package pl.edu.agh.iosr.crawler.crawlerservice.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import pl.edu.agh.iosr.crawler.crawlerservice.domain.SimpleBook;

import java.util.List;

/**
 * Created by user on 09.12.2017.
 */
@FeignClient(name = "agregate-service")
public interface AgregateClient {

    @PostMapping("/match")
    void matchBook(List<SimpleBook> books);
}
