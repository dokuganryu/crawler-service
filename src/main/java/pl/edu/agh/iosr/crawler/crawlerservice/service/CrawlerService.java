package pl.edu.agh.iosr.crawler.crawlerservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.edu.agh.iosr.crawler.crawlerservice.domain.BookPortion;
import pl.edu.agh.iosr.crawler.crawlerservice.domain.SimpleBook;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 07.12.2017.
 */
@Service
public class CrawlerService {

    private static final Logger LOG = LoggerFactory.getLogger(CrawlerService.class);
    private final String USER_AGENT = "Mozilla/5.0";
    private String baseUrl ="http://data.bn.org.pl/api/bibs.json?limit=100&";

    public ArrayList<SimpleBook> scrapUrl(String url,boolean follow) throws IOException, JSONException {
        ArrayList<SimpleBook> bookList= new ArrayList<>();
        String nextPageUrl = url;
        LOG.info("Starting scrapping...");
        LOG.info("main url: " + nextPageUrl);
        while(nextPageUrl.contains("http:")){
            LOG.info("Scrapping page:"+ nextPageUrl);
            URL obj = new URL(nextPageUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject json= new JSONObject(response.toString());
            if (follow)
                nextPageUrl = json.getString("nextPage");
            else
                nextPageUrl = "";
            JSONArray bibs = json.getJSONArray("bibs");
            ObjectMapper objectMapper = new ObjectMapper();
            bookList.addAll(objectMapper.readValue(bibs.toString(), TypeFactory.defaultInstance().constructCollectionType(List.class, SimpleBook.class)));
        }
        LOG.info("finished scrapping.");
        LOG.info("Elements: "+ bookList.size());

        //removing unnecessary '/' from title
        for (SimpleBook record : bookList) {
            record.setTitle(record.getTitle().replaceAll("/",""));
        }

    return bookList;
    }

    public BookPortion scrapUrlPortion(String url, Integer pages) throws IOException, JSONException {
        ArrayList<SimpleBook> bookList= new ArrayList<>();
        String nextPageUrl = url;
        int counter = pages;
        LOG.info("Starting scrapping...");
        LOG.info("main url: " + nextPageUrl);
        while(nextPageUrl.contains("http:")&&counter>0){
            counter--;
         //   LOG.info("Scrapping page:"+ nextPageUrl);
            URL obj = new URL(nextPageUrl);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = con.getResponseCode();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            JSONObject json= new JSONObject(response.toString());
            nextPageUrl = json.getString("nextPage");
            JSONArray bibs = json.getJSONArray("bibs");
            ObjectMapper objectMapper = new ObjectMapper();
            bookList.addAll(objectMapper.readValue(bibs.toString(), TypeFactory.defaultInstance().constructCollectionType(List.class, SimpleBook.class)));
        }
        LOG.info("finished scrapping.");
        LOG.info("Elements: "+ bookList.size());

        //removing unnecessary '/' from title
        for (SimpleBook record : bookList) {
            record.setTitle(record.getTitle().replaceAll("/",""));
        }

        BookPortion portion = new BookPortion();
        if(nextPageUrl.contains("http:")){
            String sinceId = nextPageUrl.substring(nextPageUrl.lastIndexOf("sinceId=")+8);
            portion.setNextId(sinceId);
            LOG.info("Sending next page id: "+sinceId);
        }else{
            LOG.info("All books scrapped!");
            portion.setNextId("END");
        }
        portion.setBookList(bookList);
        return portion;

    }

}
