package pl.edu.agh.iosr.crawler.crawlerservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.codehaus.jettison.json.JSONObject;
import org.json.JSONException;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import pl.edu.agh.iosr.crawler.crawlerservice.domain.BookPortion;
import pl.edu.agh.iosr.crawler.crawlerservice.domain.SimpleBook;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@Category(IntegrationTest.class)
public class CrawlerServiceApplicationTests {

//	public static DockerComposeRule docker = DockerComposeRule.builder()
//			.file("src/test/resources/docker-compose.yml")
//			.waitingForService(Eureka,Config)
//			.build();

	TestRestTemplate restTemplate = new TestRestTemplate();
	HttpHeaders headers = new HttpHeaders();
	private static final Logger LOG = LoggerFactory.getLogger(CrawlerServiceApplicationTests.class);

	@Test
	public void testApiResponse() throws JSONException {
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange("http://localhost:9000/title:testytest", HttpMethod.GET, entity, String.class);
		LOG.info(response.toString());
		LOG.info("Response: " + response.getStatusCodeValue());
		assertEquals(response.getStatusCodeValue(), 200);
		JSONAssert.assertEquals("[]", response.getBody(), false);

	}

	@Test
	public void testBookPortionStructure() throws JsonProcessingException, JSONException {
		ObjectMapper mapper = new ObjectMapper();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		BookPortion expected = new BookPortion();
		expected.setNextId("END");
		expected.setBookList(new ArrayList<SimpleBook>());

		ResponseEntity<String> response = restTemplate.exchange("http://localhost:9000/year:996/0", HttpMethod.GET, entity, String.class);
		JSONAssert.assertEquals(mapper.writeValueAsString(expected), response.getBody(), false);
	}


	@Test
	public void testBookPortionElements() throws IOException {
	HttpEntity<String> entity = new HttpEntity<String>(null, headers);
	ResponseEntity<String> response = restTemplate.exchange("http://localhost:9000/year:1990/0", HttpMethod.GET, entity, String.class);
	ObjectMapper mapper = new ObjectMapper();
	BookPortion responsePortion = mapper.readValue(response.getBody(),BookPortion.class);
	assertEquals(responsePortion.getBookList().size(),100);

	}

	@Test
	public void testBookPortionContent() throws JsonProcessingException, JSONException {
		ObjectMapper mapper = new ObjectMapper();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		BookPortion expected = new BookPortion();
		expected.setNextId("5103666");
		ArrayList<SimpleBook> books = new ArrayList<SimpleBook>();
		books.add(0,new SimpleBook("[Graduale cisterciense]. Biblioteka Baworowskich we Lwowie","Biblioteka Baworowskich (Lwów).","","1250"));
		books.add(1,new SimpleBook("Biblia  (lat). [Biblia. Testamentum Vetus et Novum cum prologis].","","","1250"));
		books.add(2,new SimpleBook("Sententiarum libri IV  Sententiarum libri IV  Variae notae theologicae. Variae notae theologicae.   Biblioteka Baworowskich we Lwowie","Piotr Lombard (ok. 1095-1159) Biblioteka Baworowskich (Lwów).","","1250"));
		books.add(3,new SimpleBook("Biblia. (łac.) [Biblia] : [Testamentum Vetus et Novum cum prologis (Ex 37-)]. Testamentum Vetus et Novum cum prologis Ex 37- Biblioteka Baworowskich we Lwowie","Biblioteka Baworowskich (Lwów).","","1250"));
		books.add(4,new SimpleBook("Biblia  (lat). [Biblia. Testamentum Vetus et Novum]. Biblioteka Baworowskich we Lwowie","Bandtkie, Jan Wincenty (1783-1846). Biblioteka Baworowskich (Lwów).","","1250"));
		expected.setBookList(books);


		ResponseEntity<String> response = restTemplate.exchange("http://localhost:9000/year:1250/0",HttpMethod.GET,entity, String.class);
		JSONAssert.assertEquals( mapper.writeValueAsString(expected), response.getBody(), false);

	}

	@Test
	public void testPortionCount() throws IOException {
		int expected = 38;
		int counter = 0;
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);
		ObjectMapper mapper = new ObjectMapper();
		String id = "0";
		while(!id.equals("END")){
			ResponseEntity<String> response = restTemplate.exchange("http://localhost:9000/year:1870/"+id, HttpMethod.GET, entity, String.class);
			BookPortion responsePortion = mapper.readValue(response.getBody(),BookPortion.class);
			id = responsePortion.getNextId();
			counter++;
		}
		assertEquals(counter, expected);


	}
}

